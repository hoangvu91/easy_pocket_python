# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime


class Currency(models.Model):
    currency = models.CharField(max_length=3, blank=False)

    def __str__(self):
        return str(self.currency)


class Bill(models.Model):
    submit_date = models.DateTimeField(default=datetime.now(), blank=False)
    amount = models.DecimalField(max_digits=15, decimal_places=2, blank=False)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    purpose = models.CharField(max_length=255, blank=False)
    note = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return str(self.amount)
