from rest_framework import serializers
from .models import Bill

class BillSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bill
        fields = ('id', 'submit_date', 'amount', 'currency', 'purpose', 'note')
