# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from bills.models import Bill, Currency

admin.site.register(Bill)
admin.site.register(Currency)
