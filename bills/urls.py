from django.conf.urls import include, url
from .views import bill_list, bill_detail

urlpatterns = (
    url(r'^$', bill_list, name='bill_list'),
    url(r'^(?P<pk>[0-9]+)$', bill_detail, name='bill_detail'),
)
