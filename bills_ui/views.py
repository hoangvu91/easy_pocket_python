import json

import requests
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse
from django.template import loader


def bills(request):

    template = loader.get_template('bills_ui/bill_list.html')
    context = {
    }
    return HttpResponse(template.render(context, request))


@require_http_methods(['GET', 'POST'])
def create(request):

    if request.method == 'GET':
        template = loader.get_template('bills_ui/create_bill.html')
        context = {
        }
        return HttpResponse(template.render(context, request))

    if request.method == 'POST':
        data = {
            "submit_date": str(request.POST.get('submit_date')) + " 00:00:00",
            "amount": request.POST.get('amount'),
            "currency": 1,
            "purpose": request.POST.get('purpose'),
            "note": request.POST.get('note')
        }

        r = requests.post('http://localhost:8000/api/bills/', data=data)

        if r.status_code == 201:
            return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            return HttpResponse('Oh god!!!, what the fuck is wrong now.')


def find_bill_by_id(request):
    template = loader.get_template('bills_ui/index.html')
    r = requests.get('http://localhost:8000/api/bills/' + request.GET.get(''))
    context = {
        'bill': r.json(),
    }

    return HttpResponse(template.render(context, request))
