# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from . import views

app_name = "bill_ui"

urlpatterns = (
    url(r'^$|^list$', views.bills, name='list'),
    url(r'^create', views.create, name='create'),
    url(r'^getBill$', views.find_bill_by_id, name='find_bill_by_id'),
    # url(r'^(?P<pk>[0-9]+)$', FindBillByID, name='find_bill_by_id'),
)
