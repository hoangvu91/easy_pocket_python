# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class BillsUiConfig(AppConfig):
    name = 'bills_ui'
