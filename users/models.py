# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class User (models.Model) :
	email = models.CharField(max_length=255, blank=False)
	password = models.CharField(max_length=255, blank=False)