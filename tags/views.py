# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import viewsets
from .serializers import TagSerializer
from .models import Tag

from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
import json
@api_view(['GET', 'POST'])
def tag_list(request):
    if request.method == 'GET':
        tags = Tag.objects.all()
        serializer = TagSerializer(tags, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        mutable = request.POST._mutable
        request.POST._mutable = True
        request.POST['tag_name'] =request.POST['tag_name'].lower()
        request.POST._mutable = mutable
        serializer = TagSerializer(data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def tag_detail(request, pk):
    try:
        tag = Tag.objects.get(pk=pk)
    except Tag.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TagSerializer(tag)
        return Response(serializer.data)

    elif request.method == 'PUT':
        mutable = request.data._mutable
        request.data._mutable = True
        request.data['tag_name'] = request.data['tag_name'].lower()
        request.data._mutable = mutable
        serializer = TagSerializer(tag, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(
                serilizer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        tag.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def tag_list_by_name(request, tagname):
    try:
        tags = Tag.objects.filter(tag_name=tagname)
    except Tag.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TagSerializer(tags, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def tag_list_by_bill(request, bill):
    try:
        tags = Tag.objects.filter(bill=bill)
    except Tag.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TagSerializer(tags, many=True)
        return Response(serializer.data)
