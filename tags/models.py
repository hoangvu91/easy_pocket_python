# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime
from bills.models import Bill

class Tag(models.Model):
	tag_name = models.CharField(max_length=255, blank=False)
	bill = models.ForeignKey(Bill)
