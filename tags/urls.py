from django.conf.urls import include, url
from .views import tag_list, tag_detail, tag_list_by_name, tag_list_by_bill

urlpatterns = (
    url(r'^$', tag_list, name='tag_list'),
    url(r'^(?P<pk>[0-9]+)$', tag_detail, name='tag_list'),
    url(r'^(?P<tagname>\w+)/$', tag_list_by_name, name='tag_list_by_name'),
    url(r'^bills/(?P<bill>[0-9]+)/$', tag_list_by_bill, name='tag_list_by_bill'),
)
